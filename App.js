import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";

function getCooktail() {
  const urlCooktailApi =
    "www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail";

  const axios = require("axios");

  axios.get(urlCooktailApi).then(function (dataRetrieved) {
    console.log(dataRetrieved);
  });
}

export default function App() {
  return <View style={styles.container}></View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
